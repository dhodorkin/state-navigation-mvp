package com.yavin.statenavigationmvp.fragments.mvp_settings.di

import com.yavin.statenavigationmvp.common.di.PerFragment
import com.yavin.statenavigationmvp.common.di.components.FragmentComponent
import com.yavin.statenavigationmvp.fragments.mvp_settings.ui.SettingsFragment
import dagger.Component

@PerFragment
@Component(modules = [SettingsFrModule::class])
interface SettingsFrComponent : FragmentComponent {
    fun inject(fr: SettingsFragment?)
}
package com.yavin.statenavigationmvp.fragments.mvp_settings.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.yavin.statenavigationmvp.R
import com.yavin.statenavigationmvp.common.MvpBaseFragment
import com.yavin.statenavigationmvp.common.navigation.StateNavigator
import com.yavin.statenavigationmvp.fragments.mvp_settings.SettingsPresenter
import com.yavin.statenavigationmvp.fragments.mvp_settings.di.DaggerSettingsFrComponent
import com.yavin.statenavigationmvp.fragments.mvp_settings.di.SettingsFrComponent
import com.yavin.statenavigationmvp.fragments.mvp_settings.di.SettingsFrModule
import com.yavin.statenavigationmvp.mvp_for_result.ForResultScreen.Companion.REQUEST_RANDOM_INT
import com.yavin.statenavigationmvp.mvp_for_result.ui.ForResultActivity
import kotlinx.android.synthetic.main.settings_fragment.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject


class SettingsFragment :
    SettingsViewContract,
    MvpBaseFragment<SettingsFrComponent>() {

    @Inject
    @InjectPresenter
    lateinit var presenter: SettingsPresenter

    @ProvidePresenter
    fun providePresenter(): SettingsPresenter = presenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.settings_fragment, container, false)
    }

    private lateinit var navController: NavController

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigation()

        navController = findNavController()
    }

    override fun showMsg(msg: String) {
        Toast.makeText(requireActivity(), msg, Toast.LENGTH_SHORT).show()
    }

    private fun initNavigation() {
        toNextBtn.setOnClickListener { presenter.onFinishPress() }
        toOneBtn.setOnClickListener { presenter.onStepPress() }
        startActivityForResultBtn.setOnClickListener { presenter.startForResultPress() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Toast.makeText(requireActivity(), requestCode.toString(), Toast.LENGTH_SHORT).show()
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) { // hide it in presenter -> navigator
//        presenter.onActivityResult(requestCode, resultCode, data)
//        super.onActivityResult(requestCode, resultCode, data)
//    }

    override fun buildComponent(): SettingsFrComponent {
        return DaggerSettingsFrComponent.builder()
            .settingsFrModule(SettingsFrModule(StateNavigator(requireActivity(), findNavController())))
            .build()
    }

    override fun setupDI(component: SettingsFrComponent?) {
        component?.inject(this)
    }
}





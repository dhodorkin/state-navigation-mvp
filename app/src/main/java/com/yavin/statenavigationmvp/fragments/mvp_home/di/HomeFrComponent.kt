package com.yavin.statenavigationmvp.fragments.mvp_home.di

import com.yavin.statenavigationmvp.common.di.PerFragment
import com.yavin.statenavigationmvp.common.di.components.FragmentComponent
import com.yavin.statenavigationmvp.fragments.mvp_home.ui.HomeFragment
import dagger.Component

@PerFragment
@Component(modules = [HomeFrModule::class])
interface HomeFrComponent : FragmentComponent {
    fun inject(fr: HomeFragment?)
}
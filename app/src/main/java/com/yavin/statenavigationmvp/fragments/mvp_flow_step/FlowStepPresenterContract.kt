package com.yavin.statenavigationmvp.fragments.mvp_flow_step

interface FlowStepPresenterContract {
    fun onNextPress()
    fun onSettingsPress()
}
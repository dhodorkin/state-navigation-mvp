package com.yavin.statenavigationmvp.fragments.mvp_flow_step.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.yavin.statenavigationmvp.R
import com.yavin.statenavigationmvp.common.MvpBaseFragment
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.FlowStepPresenter
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.di.DaggerFlowStepFrComponent
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.di.FlowStepFrComponent
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.di.FlowStepFrModule
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

class FlowStepFragment :
    FlowStepViewContract,
    MvpBaseFragment<FlowStepFrComponent>() {

    @Inject
    @InjectPresenter
    lateinit var presenter: FlowStepPresenter

    @ProvidePresenter
    fun providePresenter(): FlowStepPresenter = presenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val safeArgs: FlowStepFragmentArgs by navArgs()

        return when (safeArgs.flowStepNumber) {
            2 -> inflater.inflate(R.layout.flow_step_two_fragment, container, false)
            else -> inflater.inflate(R.layout.flow_step_one_fragment, container, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigation(view)
    }

    private fun initNavigation(view: View) {
        view.findViewById<Button>(R.id.toNextBtn).setOnClickListener {
            presenter.onNextPress()
        }

        view.findViewById<Button>(R.id.settings_button)?.setOnClickListener {
            presenter.onSettingsPress()
        }
    }

    override fun buildComponent(): FlowStepFrComponent {
        return DaggerFlowStepFrComponent.builder()
            .flowStepFrModule(FlowStepFrModule(findNavController()))
            .build()
    }

    override fun setupDI(component: FlowStepFrComponent?) {
        component?.inject(this)
    }
}

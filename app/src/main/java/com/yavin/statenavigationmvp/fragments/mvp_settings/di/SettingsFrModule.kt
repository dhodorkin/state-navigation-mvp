package com.yavin.statenavigationmvp.fragments.mvp_settings.di

import com.yavin.statenavigationmvp.common.navigation.StateNavigator
import com.yavin.statenavigationmvp.fragments.mvp_settings.SettingsPresenter
import dagger.Module
import dagger.Provides

@Module
class SettingsFrModule (
    private val stateNavigator: StateNavigator
) {

    @Provides
    fun provideSettingsPresenter() : SettingsPresenter {
        return SettingsPresenter(stateNavigator)
    }

}
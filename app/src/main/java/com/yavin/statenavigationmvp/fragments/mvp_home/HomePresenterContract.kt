package com.yavin.statenavigationmvp.fragments.mvp_home

interface HomePresenterContract {
    fun onNextPress()
    fun onSettingsPress()
}
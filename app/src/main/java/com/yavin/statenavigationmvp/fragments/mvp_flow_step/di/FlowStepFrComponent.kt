package com.yavin.statenavigationmvp.fragments.mvp_flow_step.di

import com.yavin.statenavigationmvp.common.di.PerFragment
import com.yavin.statenavigationmvp.common.di.components.FragmentComponent
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.ui.FlowStepFragment
import dagger.Component

@PerFragment
@Component(modules = [FlowStepFrModule::class])
interface FlowStepFrComponent : FragmentComponent {
    fun inject(fr: FlowStepFragment?)
}
package com.yavin.statenavigationmvp.fragments.mvp_settings

import android.content.Intent

interface SettingsPresenterContract {
    fun onFinishPress()
    fun startForResultPress()
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    fun onStepPress()
}
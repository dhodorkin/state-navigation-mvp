package com.yavin.statenavigationmvp.fragments.mvp_settings.ui

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

interface SettingsViewContract : MvpView {

    @OneExecution
    fun showMsg(msg: String)
}
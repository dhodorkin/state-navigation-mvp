package com.yavin.statenavigationmvp.fragments.mvp_home.di

import androidx.navigation.NavController
import com.yavin.statenavigationmvp.fragments.mvp_home.HomePresenter
import dagger.Module
import dagger.Provides

@Module
class HomeFrModule(
    private val navController: NavController
) {

    @Provides
    fun provideHomePresenter() : HomePresenter {
        return HomePresenter(navController)
    }

}
package com.yavin.statenavigationmvp.fragments.mvp_home

import androidx.navigation.NavController
import com.yavin.statenavigationmvp.fragments.mvp_home.ui.HomeFragmentDirections
import com.yavin.statenavigationmvp.fragments.mvp_home.ui.HomeViewContract
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class HomePresenter @Inject constructor(
    private val navController: NavController
) :
    MvpPresenter<HomeViewContract>(),
    HomePresenterContract {

    override fun onNextPress() {
        val flowStepNumberArg = 1
        navController.navigate(HomeFragmentDirections.nextScreen(flowStepNumberArg))
    }

    override fun onSettingsPress() {
        navController.navigate(HomeFragmentDirections.globalSettingsFragment())
    }
}
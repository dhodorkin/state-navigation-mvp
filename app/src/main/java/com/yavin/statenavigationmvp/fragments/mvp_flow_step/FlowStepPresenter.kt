package com.yavin.statenavigationmvp.fragments.mvp_flow_step

import androidx.navigation.NavController
import com.yavin.statenavigationmvp.R
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.ui.FlowStepViewContract
import com.yavin.statenavigationmvp.fragments.mvp_settings.ui.SettingsFragmentDirections
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class FlowStepPresenter @Inject constructor(
    private val navController: NavController
) :
    MvpPresenter<FlowStepViewContract>(),
    FlowStepPresenterContract {

    override fun onNextPress() {
        navController.navigate(R.id.next_screen)
    }

    override fun onSettingsPress() {
        navController.navigate(SettingsFragmentDirections.globalSettingsFragment())
    }
}
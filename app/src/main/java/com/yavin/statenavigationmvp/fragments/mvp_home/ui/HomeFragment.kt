package com.yavin.statenavigationmvp.fragments.mvp_home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.yavin.statenavigationmvp.R
import com.yavin.statenavigationmvp.common.MvpBaseFragment
import com.yavin.statenavigationmvp.fragments.mvp_home.HomePresenter
import com.yavin.statenavigationmvp.fragments.mvp_home.di.DaggerHomeFrComponent
import com.yavin.statenavigationmvp.fragments.mvp_home.di.HomeFrComponent
import com.yavin.statenavigationmvp.fragments.mvp_home.di.HomeFrModule
import kotlinx.android.synthetic.main.home_fragment.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

class HomeFragment :
    HomeViewContract,
    MvpBaseFragment<HomeFrComponent>() {

    @Inject
    @InjectPresenter
    lateinit var presenter: HomePresenter

    @ProvidePresenter
    fun providePresenter(): HomePresenter = presenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNavigation()
    }

    private fun initNavigation() {
        toNextBtn.setOnClickListener { presenter.onNextPress() }
        toSettingsBtn.setOnClickListener { presenter.onSettingsPress() }
    }

    override fun buildComponent(): HomeFrComponent {
        return DaggerHomeFrComponent.builder()
            .homeFrModule(HomeFrModule(findNavController()))
            .build()
    }

    override fun setupDI(component: HomeFrComponent?) {
        component?.inject(this)
    }
}
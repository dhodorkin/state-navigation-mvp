package com.yavin.statenavigationmvp.fragments.mvp_flow_step.di

import androidx.navigation.NavController
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.FlowStepPresenter
import dagger.Module
import dagger.Provides


@Module
class FlowStepFrModule(
    private val navController: NavController
) {

    @Provides
    fun provideFlowStepPresenter() : FlowStepPresenter {
        return FlowStepPresenter(navController)
    }

}
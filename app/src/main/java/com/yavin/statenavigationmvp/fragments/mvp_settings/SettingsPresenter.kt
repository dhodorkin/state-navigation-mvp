package com.yavin.statenavigationmvp.fragments.mvp_settings

import android.app.Activity
import android.content.Intent
import android.widget.Toast
import androidx.navigation.NavController
import com.yavin.statenavigationmvp.common.navigation.StateNavigator
import com.yavin.statenavigationmvp.fragments.mvp_flow_step.ui.FlowStepFragmentDirections
import com.yavin.statenavigationmvp.fragments.mvp_settings.ui.SettingsFragmentDirections
import com.yavin.statenavigationmvp.fragments.mvp_settings.ui.SettingsViewContract
import com.yavin.statenavigationmvp.mvp_for_result.ForResultScreen
import com.yavin.statenavigationmvp.mvp_for_result.ui.ForResultActivity
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class SettingsPresenter @Inject constructor(
    private val stateNavigator: StateNavigator
) :
    MvpPresenter<SettingsViewContract>(),
    SettingsPresenterContract {

    override fun onFinishPress() {
        stateNavigator.navController.navigate(SettingsFragmentDirections.toHome())
    }

    override fun startForResultPress() {
        stateNavigator.startForResult(ForResultScreen())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == ForResultScreen.REQUEST_RANDOM_INT) {
            if (resultCode == Activity.RESULT_OK) {
                val randInt = data?.getIntExtra(ForResultScreen.REQUEST_RANDOM_INT_DATA_TAG, 0)
                viewState.showMsg(randInt.toString())
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                viewState.showMsg("canceled")
            }
        }
    }

    override fun onStepPress() {
        stateNavigator.navController.navigate(FlowStepFragmentDirections.globalStepOneFragment()) //1,2
    }
}
package com.yavin.statenavigationmvp

import androidx.multidex.MultiDexApplication
import com.yavin.statenavigationmvp.common.di.components.ApplicationComponent
import com.yavin.statenavigationmvp.common.di.components.DaggerApplicationComponent
import com.yavin.statenavigationmvp.common.di.modules.ApplicationModule

class StateNavApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        initializeInjector()
    }

    private fun initializeInjector() {
        appComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    companion object {
        var appComponent: ApplicationComponent? = null
            private set
    }
}
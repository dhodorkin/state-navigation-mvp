package com.yavin.statenavigationmvp.common.di.components

import android.content.Context
import com.yavin.statenavigationmvp.common.di.modules.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    //Exposed to sub-graphs.
    fun context(): Context?
}
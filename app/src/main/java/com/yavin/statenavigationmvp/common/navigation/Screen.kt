package com.yavin.statenavigationmvp.common.navigation

sealed class Screen

data class HomeScreen(private val screenId: Int) : Screen()
data class FlowScreen(val flowNumber: Int) : Screen()

package com.yavin.statenavigationmvp.common

import android.os.Bundle
import com.yavin.statenavigationmvp.StateNavApplication
import com.yavin.statenavigationmvp.common.di.HasComponent
import com.yavin.statenavigationmvp.common.di.components.ActivityComponent
import com.yavin.statenavigationmvp.common.di.components.ApplicationComponent
import com.yavin.statenavigationmvp.common.di.modules.ActivityModule
import moxy.MvpAppCompatActivity

abstract class MvpBaseActivity<C : ActivityComponent> : MvpAppCompatActivity(),
    HasComponent<C> {

    override lateinit var component: C

    override fun onCreate(savedInstanceState: Bundle?) {
        component = buildComponent()
        setupDI(component)
        super.onCreate(savedInstanceState)
    }

    protected val applicationComponent: ApplicationComponent?
        get() = StateNavApplication.appComponent

    protected val activityModule: ActivityModule
        get() = ActivityModule(this)

    protected abstract fun buildComponent(): C
    protected abstract fun setupDI(component: C)
}
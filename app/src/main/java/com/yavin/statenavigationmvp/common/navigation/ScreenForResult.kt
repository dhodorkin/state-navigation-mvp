package com.yavin.statenavigationmvp.common.navigation

import android.content.Intent
import androidx.fragment.app.FragmentActivity

interface ScreenForResult {
    fun getIntent(fragmentActivity: FragmentActivity) : Intent
    fun getRequestId() : Int
    fun getDataKey(): String
}



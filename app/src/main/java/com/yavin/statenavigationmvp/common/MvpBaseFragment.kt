package com.yavin.statenavigationmvp.common

import android.os.Bundle
import com.yavin.statenavigationmvp.common.di.HasComponent
import com.yavin.statenavigationmvp.common.di.components.FragmentComponent
import moxy.MvpAppCompatFragment

abstract class MvpBaseFragment<C : FragmentComponent> : MvpAppCompatFragment(),
    HasComponent<C> {

    override lateinit var component: C

    override fun onCreate(savedInstanceState: Bundle?) {
        component = buildComponent()
        setupDI(component)
        super.onCreate(savedInstanceState)
    }

    protected abstract fun buildComponent(): C
    protected abstract fun setupDI(component: C?)
}
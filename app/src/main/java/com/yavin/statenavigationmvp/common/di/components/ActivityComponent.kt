package com.yavin.statenavigationmvp.common.di.components

import android.app.Activity
import com.yavin.statenavigationmvp.common.di.PerActivity
import com.yavin.statenavigationmvp.common.di.modules.ActivityModule
import dagger.Component

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [ActivityModule::class]
)
interface ActivityComponent {
    //Exposed to sub-graphs.
    fun activity(): Activity?
}
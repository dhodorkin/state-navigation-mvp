package com.yavin.statenavigationmvp.common.navigation

import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import com.yavin.statenavigationmvp.fragments.mvp_settings.ui.SettingsFragmentDirections

class StateNavigator(
    private val fragmentActivity: FragmentActivity,
    val navController: NavController
) : IStateNavigator {
    override fun navigateTo(screen: Screen) {
        navController.navigate(SettingsFragmentDirections.toHome()) //
    }

    override fun navigateBack() {
        TODO("Not yet implemented")
    }

    override fun navigateBackTo(screen: Screen) {
        TODO("Not yet implemented")
    }

    override fun navigateToRoot() {
        TODO("Not yet implemented")
    }

    override fun startForResult(screen: ScreenForResult) {
        fragmentActivity.startActivityForResult(
            screen.getIntent(fragmentActivity),
            screen.getRequestId())
    }
}
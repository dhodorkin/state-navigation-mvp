package com.yavin.statenavigationmvp.common.di.modules

import android.content.Context
import com.yavin.statenavigationmvp.StateNavApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: StateNavApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return application
    }

}
package com.yavin.statenavigationmvp.common.navigation

interface IStateNavigator {

    fun navigateTo(screen: Screen)
    fun navigateBack()
    fun navigateBackTo(screen: Screen)
    fun navigateToRoot()
    fun startForResult(screen: ScreenForResult
//                       , simpleCallback
    )
}
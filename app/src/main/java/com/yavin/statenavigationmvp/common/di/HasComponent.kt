package com.yavin.statenavigationmvp.common.di

interface HasComponent<C> {
    val component: C
}
package com.yavin.statenavigationmvp.common.di.modules

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import com.yavin.statenavigationmvp.common.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    @PerActivity
    fun activity(): Activity {
        return activity
    }

}
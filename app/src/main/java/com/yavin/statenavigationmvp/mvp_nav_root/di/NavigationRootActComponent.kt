package com.yavin.statenavigationmvp.mvp_nav_root.di

import com.yavin.statenavigationmvp.common.di.PerActivity
import com.yavin.statenavigationmvp.common.di.components.ActivityComponent
import com.yavin.statenavigationmvp.common.di.components.ApplicationComponent
import com.yavin.statenavigationmvp.common.di.modules.ActivityModule
import com.yavin.statenavigationmvp.mvp_nav_root.ui.NavigationRootActivity
import dagger.Component

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [
        ActivityModule::class
//        , NavigationRootScreenActModule::class
    ]
)
interface NavigationRootActComponent : ActivityComponent {
    fun inject(activity: NavigationRootActivity?)
}
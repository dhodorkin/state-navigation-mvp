package com.yavin.statenavigationmvp.mvp_nav_root

import com.yavin.statenavigationmvp.mvp_nav_root.ui.NavigateRootViewContract
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class NavigateRootPresenter @Inject constructor()
    : MvpPresenter<NavigateRootViewContract>(),
    NavigateRootPresenterContract {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        init()
    }

    private fun init() {
        viewState.setDescriptionText("Root activity for NAC. ^Fragment^")
    }

    override fun onNextPress() {

    }

}
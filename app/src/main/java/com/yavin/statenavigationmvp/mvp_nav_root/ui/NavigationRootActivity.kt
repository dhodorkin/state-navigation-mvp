package com.yavin.statenavigationmvp.mvp_nav_root.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.yavin.statenavigationmvp.R
import com.yavin.statenavigationmvp.common.MvpBaseActivity
import com.yavin.statenavigationmvp.mvp_nav_root.NavigateRootPresenter
import com.yavin.statenavigationmvp.mvp_nav_root.di.DaggerNavigationRootActComponent
import com.yavin.statenavigationmvp.mvp_nav_root.di.NavigationRootActComponent
import kotlinx.android.synthetic.main.activity_nav_root.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

class NavigationRootActivity :
    NavigateRootViewContract,
    MvpBaseActivity<NavigationRootActComponent>()
{

    @Inject
    @InjectPresenter
    lateinit var presenter: NavigateRootPresenter

    @ProvidePresenter
    fun providePresenter(): NavigateRootPresenter = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav_root)
    }

    override fun setDescriptionText(text: String) {
        descriptionTitleTv.text = text
    }

    override fun buildComponent(): NavigationRootActComponent {
        /* TODO inject navController here...
        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment? ?: return
        val navController = host.navController
        */
        return DaggerNavigationRootActComponent.builder()
            .applicationComponent(applicationComponent)
            .activityModule(activityModule)
            .build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Toast.makeText(this, requestCode.toString(), Toast.LENGTH_SHORT).show()
    }

    override fun setupDI(component: NavigationRootActComponent) {
        component.inject(this)
    }
}

package com.yavin.statenavigationmvp.mvp_nav_root

interface NavigateRootPresenterContract {
    fun onNextPress()
}
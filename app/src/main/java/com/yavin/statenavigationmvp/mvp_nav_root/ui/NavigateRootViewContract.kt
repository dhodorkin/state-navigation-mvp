package com.yavin.statenavigationmvp.mvp_nav_root.ui

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

interface NavigateRootViewContract : MvpView {

    @OneExecution
    fun setDescriptionText(text: String)

}
package com.yavin.statenavigationmvp.mvp_for_result

interface ForResultPresenterContract {
    fun onRandomIntResultPress()
    fun onObjectResultPress()
}
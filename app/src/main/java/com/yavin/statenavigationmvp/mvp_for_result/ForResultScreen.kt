package com.yavin.statenavigationmvp.mvp_for_result

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import com.yavin.statenavigationmvp.common.navigation.ScreenForResult
import com.yavin.statenavigationmvp.mvp_for_result.ui.ForResultActivity

class ForResultScreen : ScreenForResult {

    override fun getIntent(fragmentActivity: FragmentActivity): Intent {
        return Intent(fragmentActivity, ForResultActivity::class.java)
    }

    override fun getRequestId(): Int = REQUEST_RANDOM_INT

    override fun getDataKey(): String = REQUEST_RANDOM_INT_DATA_TAG

    companion object {
        const val REQUEST_RANDOM_INT = 100
        const val REQUEST_RANDOM_INT_DATA_TAG = "random_int"
        const val REQUEST_OBJECT = 101
    }

}
package com.yavin.statenavigationmvp.mvp_for_result.di

import com.yavin.statenavigationmvp.common.di.PerActivity
import com.yavin.statenavigationmvp.common.di.components.ActivityComponent
import com.yavin.statenavigationmvp.common.di.components.ApplicationComponent
import com.yavin.statenavigationmvp.common.di.modules.ActivityModule
import com.yavin.statenavigationmvp.mvp_for_result.ui.ForResultActivity
import dagger.Component

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [
        ActivityModule::class
//        , NavigationRootScreenActModule::class
    ]
)
interface ForResultActComponent : ActivityComponent {
    fun inject(activity: ForResultActivity?)
}
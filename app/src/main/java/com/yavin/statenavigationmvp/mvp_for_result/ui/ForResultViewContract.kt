package com.yavin.statenavigationmvp.mvp_for_result.ui

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

interface ForResultViewContract : MvpView {

    @OneExecution
    fun setDescriptionText(text: String)

    @OneExecution
    fun setRandomIntValue(randomInt: Int)

    @OneExecution
    fun setRandomIntValueAsResult(randomInt: Int)
}
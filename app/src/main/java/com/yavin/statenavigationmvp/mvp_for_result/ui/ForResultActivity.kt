package com.yavin.statenavigationmvp.mvp_for_result.ui

import android.app.Activity
import android.os.Bundle
import com.yavin.statenavigationmvp.R
import com.yavin.statenavigationmvp.common.MvpBaseActivity
import com.yavin.statenavigationmvp.mvp_for_result.ForResultPresenter
import com.yavin.statenavigationmvp.mvp_for_result.ForResultScreen.Companion.REQUEST_RANDOM_INT_DATA_TAG
import com.yavin.statenavigationmvp.mvp_for_result.di.DaggerForResultActComponent
import com.yavin.statenavigationmvp.mvp_for_result.di.ForResultActComponent
import kotlinx.android.synthetic.main.activity_for_result.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject


class ForResultActivity :
    ForResultViewContract,
    MvpBaseActivity<ForResultActComponent>()
{

    @Inject
    @InjectPresenter
    lateinit var presenter: ForResultPresenter

    @ProvidePresenter
    fun providePresenter(): ForResultPresenter = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_for_result)

        simpleResultBtn.setOnClickListener { presenter.onRandomIntResultPress() }
        objectResultBtn.setOnClickListener { presenter.onObjectResultPress() }
    }

    override fun setDescriptionText(text: String) {
        descriptionTitleTv.text = text
    }

    override fun setRandomIntValue(randomInt: Int) {
        randomIntTv.text = randomInt.toString()
    }

    override fun setRandomIntValueAsResult(randomInt: Int) { // hmm
        setResult(
            Activity.RESULT_OK,
            intent.putExtra(REQUEST_RANDOM_INT_DATA_TAG, randomInt)
        )
        finish()
    }

    override fun buildComponent(): ForResultActComponent {
        return DaggerForResultActComponent.builder()
            .applicationComponent(applicationComponent)
            .activityModule(activityModule)
            .build()
    }

    override fun setupDI(component: ForResultActComponent) {
        component.inject(this)
    }

}

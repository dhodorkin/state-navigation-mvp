package com.yavin.statenavigationmvp.mvp_for_result

import com.yavin.statenavigationmvp.mvp_for_result.ui.ForResultViewContract
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject
import kotlin.random.Random

@InjectViewState
class ForResultPresenter @Inject constructor()
    : MvpPresenter<ForResultViewContract>(),
    ForResultPresenterContract {

    private var randomInt: Int = -1

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        init()
    }

    private fun init() {
        randomInt = Random.nextInt(0,99)
        viewState.setRandomIntValue(randomInt)
        viewState.setDescriptionText("Activity for result")
    }

    override fun onRandomIntResultPress() {
        viewState.setRandomIntValueAsResult(randomInt)
    }

    override fun onObjectResultPress() {
        TODO("Not yet implemented")
    }
}
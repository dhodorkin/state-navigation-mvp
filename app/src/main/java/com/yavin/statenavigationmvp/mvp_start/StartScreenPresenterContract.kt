package com.yavin.statenavigationmvp.mvp_start

interface StartScreenPresenterContract {
    fun onNextPress()
}
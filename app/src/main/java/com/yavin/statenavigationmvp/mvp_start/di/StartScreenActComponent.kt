package com.yavin.statenavigationmvp.mvp_start.di

import com.yavin.statenavigationmvp.common.di.PerActivity
import com.yavin.statenavigationmvp.common.di.components.ActivityComponent
import com.yavin.statenavigationmvp.common.di.components.ApplicationComponent
import com.yavin.statenavigationmvp.common.di.modules.ActivityModule
import com.yavin.statenavigationmvp.mvp_start.ui.StartScreenActivity
import dagger.Component

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [
        ActivityModule::class
//        , StartScreenActModule::class
    ]
)
interface StartScreenActComponent : ActivityComponent {
    fun inject(activity: StartScreenActivity?)
}
package com.yavin.statenavigationmvp.mvp_start.ui

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution

interface StartScreenViewContract : MvpView {

    @OneExecution
    fun setDescriptionText(text: String)

    @OneExecution
    fun navigateToNext()

}
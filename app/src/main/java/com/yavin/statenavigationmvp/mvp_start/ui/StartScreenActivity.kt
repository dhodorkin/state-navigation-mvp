package com.yavin.statenavigationmvp.mvp_start.ui

import android.content.Intent
import android.os.Bundle
import com.yavin.statenavigationmvp.R
import com.yavin.statenavigationmvp.common.MvpBaseActivity
import com.yavin.statenavigationmvp.mvp_nav_root.ui.NavigationRootActivity
import com.yavin.statenavigationmvp.mvp_start.StartScreenPresenter
import com.yavin.statenavigationmvp.mvp_start.di.DaggerStartScreenActComponent
import com.yavin.statenavigationmvp.mvp_start.di.StartScreenActComponent
import kotlinx.android.synthetic.main.activity_start.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import javax.inject.Inject

class StartScreenActivity :
    StartScreenViewContract,
    MvpBaseActivity<StartScreenActComponent>()  {

    @Inject
    @InjectPresenter
    lateinit var presenter: StartScreenPresenter

    @ProvidePresenter
    fun providePresenter(): StartScreenPresenter = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        initUI()
    }

    private fun initUI() {
        toNextBtn.setOnClickListener { presenter.onNextPress() }
    }

    override fun setDescriptionText(text: String) {
        descriptionTitleTv.text = text
    }

    override fun navigateToNext() {
        startActivity(Intent(this, NavigationRootActivity::class.java))
    }

    override fun buildComponent(): StartScreenActComponent {
        return DaggerStartScreenActComponent.builder()
            .applicationComponent(applicationComponent)
//            .startScreenActModule(StartScreenActModule())
            .activityModule(activityModule)
            .build()
    }

    override fun setupDI(component: StartScreenActComponent) {
        component.inject(this)
    }
}

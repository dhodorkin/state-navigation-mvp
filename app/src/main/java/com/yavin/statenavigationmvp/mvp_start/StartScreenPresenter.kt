package com.yavin.statenavigationmvp.mvp_start

import com.yavin.statenavigationmvp.mvp_start.ui.StartScreenViewContract
import moxy.InjectViewState
import moxy.MvpPresenter
import javax.inject.Inject

@InjectViewState
class StartScreenPresenter @Inject constructor()
    : MvpPresenter<StartScreenViewContract>(), StartScreenPresenterContract {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        init()
    }

    private fun init() {
        viewState.setDescriptionText("Old fashion \"legacy\" activity")
    }

    override fun onNextPress() {
        viewState.navigateToNext()
    }
}